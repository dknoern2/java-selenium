package com.slalom.navigation;

import com.slalom.selenium.Driver;
import org.openqa.selenium.By;

class MenuSelector
{
    public static void select(String topLevelMenu, String subMenuLinkText)
    {
        Driver.getDriver().findElement(By.id(topLevelMenu)).click();
        Driver.wait(500);
        Driver.getDriver().findElement(By.linkText(subMenuLinkText)).click();
    }
}
