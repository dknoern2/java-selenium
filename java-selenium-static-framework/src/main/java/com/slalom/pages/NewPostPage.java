package com.slalom.pages;

import com.slalom.navigation.LeftNavigation;
import com.slalom.selenium.Driver;
import org.openqa.selenium.By;

public class NewPostPage
{
    public static void goTo()
    {
        LeftNavigation.Posts.AddNew.select();
    }
    
    public static CreatePostCommand createPost(String title)
    {
        return new CreatePostCommand(title);
    }

    public static class CreatePostCommand
    {
        private final String title;
        private String body;
    
        public CreatePostCommand(String title)
        {
            this.title = title;
        }

        public CreatePostCommand withBody(String body)
        {
            this.body = body;
            return this;
        }

        public void publish()
        {
            Driver.getDriver().findElement(By.id("title")).sendKeys(this.title);
            Driver.getDriver().switchTo().frame("content_ifr");
            Driver.getDriver().switchTo().activeElement().sendKeys(this.body);
            Driver.getDriver().switchTo().defaultContent();

            Driver.wait(1000);

            Driver.getDriver().findElement(By.id("publish")).click();
        }
    }
}
