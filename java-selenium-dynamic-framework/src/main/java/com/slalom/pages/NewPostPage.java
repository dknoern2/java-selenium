package com.slalom.pages;

import com.slalom.navigation.LeftNavigation;
import com.slalom.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.slalom.util.PageHelper.getDriver;

public class NewPostPage extends BasePage
{
    @FindBy(id="title")
    private WebElement titleInput;

    @FindBy(id="publish")
    private WebElement publishButton;

    public NewPostPage goTo()
    {
        LeftNavigation.Posts.AddNew.select();
        return this;
    }
    
    public CreatePostCommand createPost(String title)
    {
        return new CreatePostCommand(title);
    }

    public class CreatePostCommand
    {
        private final String title;
        private String body;
    
        public CreatePostCommand(String title)
        {
            this.title = title;
        }
    
        public CreatePostCommand withBody(String body)
        {
            this.body = body;
            return this;
        }
    
        public void publish()
        {
            titleInput.sendKeys(this.title);
            getDriver().switchTo().frame("content_ifr");

            WebElement bodyInput = getDriver().findElement(By.id("tinymce"));

            // workaround for entering text in tinymce editor
            ((JavascriptExecutor) getDriver()).executeScript(
                    "arguments[0].innerHTML = '<h1>Set text using innerHTML</h1>'", bodyInput);

            getDriver().switchTo().defaultContent();


            publishButton.click();
        }
    }
}
