package com.slalom.pages;

import com.google.common.base.Predicate;
import com.slalom.base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.slalom.selenium.TestConfiguration.BaseAddress;
import static com.slalom.util.PageHelper.getDriver;

public class LoginPage extends BasePage
{
    @FindBy(id="user_login")
    private WebElement userLogin;

    @FindBy(id="user_pass")
    private WebElement userPass;

    @FindBy(id="wp-submit")
    private WebElement loginButton;

    public LoginPage goTo()
    {
        getDriver().navigate().to(BaseAddress + "/wp-login.php");
        WebDriverWait wait = new WebDriverWait(getDriver(), 2);
        wait.until((Predicate<WebDriver>) p -> p.switchTo().activeElement().getAttribute("id").equals("user_login") );
        return this;
    }
    
    public LoginCommand loginAs(String user)
    {
        return new LoginCommand(user);
    }

    public class LoginCommand
    {
        private final String user;
        private String password;
    
        public LoginCommand(String user)
        {
            this.user = user;
        }

        public LoginCommand withPassword(String pass)
        {
            this.password = pass;
            return this;
        }
    
        public void login()
        {
            userLogin.sendKeys(this.user);
            userPass.sendKeys(this.password);
            loginButton.click();
        }
    }
}
