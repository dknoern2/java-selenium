package com.slalom.pages;

import com.slalom.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.slalom.util.PageHelper.getDriver;

public class DashboardPage extends BasePage
{
    public static boolean isAt()
    {
        WebDriverWait wait = new WebDriverWait(getDriver(), 2);
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.id("welcome-panel"))) != null;
    }
}
