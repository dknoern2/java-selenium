package com.slalom.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

import static com.slalom.selenium.Driver.noWait;
import static com.slalom.util.PageHelper.getDriver;

public class PostRows
{
    @FindBy(tagName="tr")
    private List<WebElement> postRows;

    public void deletePost(String title)
    {
        for (WebElement row : postRows)
        {
            List<WebElement> links = new ArrayList<>();
            noWait(() -> links.addAll(row.findElements(By.linkText(title))));

            if (links.size() <= 0) continue;

            Actions action = new Actions(getDriver());
            action.moveToElement(links.get(0));
            action.perform();
            row.findElement(By.className("submitdelete")).click();
            break;
        }
    }
}
