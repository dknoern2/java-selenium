package com.slalom.base;

import com.saucelabs.junit.ConcurrentParameterized;
import com.slalom.selenium.DriverConfiguration;
import com.slalom.selenium.RemoteCapabilities;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

@RunWith(ConcurrentParameterized.class)
public class ConcurrentParameterizedTestBase extends TestBase
{
    @Rule
    public TestName name = new TestName()
    {
        public String getMethodName()
        {
            return String.format("%s", super.getMethodName());
        }
    };

    /**
     * Remote web driver capabilities
     */
    private RemoteCapabilities capabilities = new RemoteCapabilities();

    /**
     * Constructs a new instance of the test.  The constructor requires three string parameters, which represent the operating
     * system, version and browser to be used when launching a Sauce VM.  The order of the parameters should be the same
     * as that of the elements within the {@link #browsersStrings()} method.
     * @param browser
     */
    public ConcurrentParameterizedTestBase(String browser)
    {
        super();
        capabilities = new RemoteCapabilities().withBrowser(browser);
    }

    protected DesiredCapabilities getCapabilities()
    {
        return capabilities.withMethodName(name.getMethodName()).withBuildTag(buildTag).build();
    }

    @Override
    public WebDriver getDriver()
    {
        return new RemoteWebDriver(getUrl(), getCapabilities());
    }

    private URL getUrl()
    {
        String urlString = getUrlString();

        try
        {
            return new URL(urlString);
        }
        catch (MalformedURLException e)
        {
            throw new RuntimeException("Invalid remote URL: " + urlString);
        }

    }
    
    protected String getUrlString()
    {
        return DriverConfiguration.RemoteURL.toString();
    }
}
