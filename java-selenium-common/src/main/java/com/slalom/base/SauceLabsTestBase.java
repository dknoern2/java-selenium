package com.slalom.base;

import com.saucelabs.common.SauceOnDemandAuthentication;
import com.saucelabs.common.SauceOnDemandSessionIdProvider;
import com.saucelabs.junit.SauceOnDemandTestWatcher;
import com.slalom.selenium.Driver;
import com.slalom.selenium.RemoteCapabilities;
import com.slalom.util.SauceHelper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class SauceLabsTestBase extends ConcurrentParameterizedTestBase implements SauceOnDemandSessionIdProvider
{
    private static final String username = System.getenv("test.sauce.user");
    private static final String accesskey = System.getenv("test.sauce.access.key");

    public static String seleniumURI;

    /**
     * Constructs a {@link SauceOnDemandAuthentication} instance using the supplied user name/access key.  To use the authentication
     * supplied by environment variables or from an external file, use the no-arg {@link SauceOnDemandAuthentication} constructor.
     */
    public SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication(username, accesskey);

    /**
     * JUnit Rule which will mark the Sauce Job as passed/failed when the test succeeds or fails.
     */
    @Rule
    public SauceOnDemandTestWatcher resultReportingTestWatcher = new SauceOnDemandTestWatcher(this, authentication);

    /**
     * Instance variable which contains the Sauce Job Id.
     */
    protected String sessionId;


    public SauceLabsTestBase(String browser)
    {
        super(browser);
    }

    /**
     * Constructs a new {@link RemoteWebDriver} instance which is configured to use the capabilities defined by the
     * {@link RemoteCapabilities#browser}, {@link RemoteCapabilities#version} and {@link RemoteCapabilities#os}
     * values, and which is configured to run against ondemand.saucelabs.com, using
     * the username and access key populated by the {@link #authentication} instance.
     *
     * @throws Exception if an error occurs during the creation of the {@link RemoteWebDriver} instance.
     */
    @Before
    public void sauceSetup()
    {
        this.sessionId = (((RemoteWebDriver) Driver.getDriver()).getSessionId()).toString();
        String message = String.format("SauceOnDemandSessionID=%1$s job-name=%2$s", this.sessionId, name.getMethodName());
        System.out.println(message);
    }

    /**
     *
     * @return the value of the Sauce Job id.
     */
    @Override
    public String getSessionId()
    {
        return sessionId;
    }

    @BeforeClass
    public static void sauceSetupClass()
    {
        //get the uri to send the commands to.
        seleniumURI = SauceHelper.buildSauceUri();
    }

    @Override
    protected DesiredCapabilities getCapabilities()
    {
        DesiredCapabilities capabilities = super.getCapabilities();
        SauceHelper.addSauceConnectTunnelId(capabilities);
        return capabilities;
    }

    @Override
    protected String getUrlString()
    {
        return "http://" + username + ":" + accesskey + seleniumURI +"/wd/hub";
    }
}
