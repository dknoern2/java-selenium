package com.slalom.base;

import com.slalom.rule.RetryRule;
import com.slalom.selenium.Driver;
import com.slalom.selenium.DriverProvider;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;

import java.util.Calendar;

import static com.slalom.util.ScreenshotHelper.takeScreenshot;

public abstract class TestBase implements DriverProvider
{
    // If available add build tag. When running under Jenkins BUILD_TAG is automatically set.
    // You can set this manually on manual runs.
    static String buildTag = System.getenv("BUILD_TAG");;

    @Rule
    public TestRule screenShotTestWatcher = new TestWatcher()
    {
        @Override
        public void failed(Throwable t, Description test)
        {
            takeScreenshot("failed/" + test.getDisplayName() + "-" + Calendar.getInstance().getTime());
        }

        @Override
        public void succeeded(Description test)
        {
            takeScreenshot("succeeded/" + test.getDisplayName() + "-" + Calendar.getInstance().getTime());
        }

        @Override
        protected void finished(Description description)
        {
            // Moved Driver.close() here because failed() gets called after @After
            Driver.close();
        }
    };

    // Tests marked @Retry will be retried
    @Rule
    public RetryRule retryRule = new RetryRule(2);

    @Before
    public void baseSetup()
    {
        Driver.initialize(this);
    }


    public WebDriver getDriver()
    {
        return null;
    }
}
