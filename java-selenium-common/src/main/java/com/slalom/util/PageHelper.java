package com.slalom.util;

import com.slalom.selenium.Driver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public final class PageHelper
{
    private PageHelper() {}

    public static WebDriver getDriver()
    {
        return Driver.getDriver();
    }

    public static <T> T getPage(Class<T> pageClass)
    {
        return PageFactory.initElements(getDriver(), pageClass);
    }
}
