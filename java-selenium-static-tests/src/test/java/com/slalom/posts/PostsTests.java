package com.slalom.posts;

import com.slalom.base.WordpressTests;
import com.slalom.pages.ListPostsPage;
import com.slalom.pages.PageType;
import com.slalom.workflows.PostCreator;
import org.junit.Assert;
import org.junit.Test;

public class PostsTests extends WordpressTests
{
    // Test demonstrates storing state by storing count of posts
    // Also demonstrates use of a workflow PageCreator
    @Test
    public void addedPostsShowUp()
    {
        // Go to posts, get current count
        ListPostsPage.goTo(PageType.Post);
        ListPostsPage.storeCount();

        // Create a new post
        PostCreator.createPost();

        // Go to posts, get count
        ListPostsPage.goTo(PageType.Post);
        Assert.assertEquals("Post counts are not equal", ListPostsPage.getPreviousPostCount() + 1, ListPostsPage.getCurrentPostCount());

        // Check for added post
        Assert.assertTrue(ListPostsPage.doesPostExistWithTitle(PostCreator.getPreviousTitle()));

        // Delete post and verify
        ListPostsPage.trashPost(PostCreator.getPreviousTitle());
        Assert.assertEquals("Post count incorrect", ListPostsPage.getPreviousPostCount(), ListPostsPage.getCurrentPostCount());
    }

    @Test
    public void canSearchPosts()
    {
        // Create new post
        PostCreator.createPost();

        // Search for post
        ListPostsPage.searchForPost(PostCreator.getPreviousTitle());

        // Check that posts shows in results
        Assert.assertTrue(ListPostsPage.doesPostExistWithTitle(PostCreator.getPreviousTitle()));
    }
}
