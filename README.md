# Java-Selenium #

This repo contains two WebDriver (**Selenium**) frameworks and corresponding test projects. The frameworks demonstrate the following:

* **PageObjects**
* **State Management**
* **Navigation**
* **Workflows**
* **Screenshots**
* **Test Rules**
* **PhantomJs (headless browser)**
* **Selenium Grid and RemoteWebDriver**
* **Parallel Tests**
* **Parameterized Tests**

Future

* **Sauce Labs Integration**

The frameworks/tests follow these principles:

1. NEVER expose browser or DOM to tests or allow tests to manipulate them
2. NEVER require tests to declare variables
3. NEVER require tests to use "new" keyword
4. NEVER require tests to manage state 
5. ALWAYS reduce parameters required
6. ALWAYS use default parameter values when possible

## Java Test Framework ##
There are two framework versions. A completely static framework uses WebDriver exclusively to access elements. 
A dynamic framework that uses Selenium page objects to initiate elements and also use WebDriver API for accessing elements.

### java-selenium-static-framework ###

All calls to framework or made through static methods. State is managed statically. As a result, tests and framework are very simple but are obviously not thread safe. Parallelization can only be achieved running tests on separate nodes. Below is an example of a test using the static framework:
```
#!java

    @Test
    public void canSearchPosts()
    {
        // Create new post
        PostCreator.createPost();

        // Search for post
        ListPostsPage.searchForPost(PostCreator.getPreviousTitle());

        // Check that posts shows in results
        Assert.assertTrue(ListPostsPage.doesPostExistWithTitle(PostCreator.getPreviousTitle()));
    }

```

These tests can be ran using the command:

./gradlew :java-selenium-static-tests:test


### java-selenium-dynamic-framework ###

The dynamic framework uses Selenium to create most pages. State is managed through ThreadLocal variables in the framework. While the tests should be thread safe, Parallelization should be performed at class level or higher as designed. An example using the dynamic framework is below:

```
#!java

    @Test
    public void canSearchPosts()
    {
        // Create new post
        PostCreator.createPost();

        // Search for post
        ListPostsPage listPostsPage = getPage(ListPostsPage.class);
        listPostsPage.searchForPost(PostCreator.getPreviousTitle());

        // Check that posts shows in results
        Assert.assertTrue(listPostsPage.doesPostExistWithTitle(PostCreator.getPreviousTitle()));
    }

```
Note, using a Wrapper/Decorator the same test wrote against the static framework can be wrote using the dynamic framework. See https://bitbucket.org/tredfield/java-selenium/commits/1d1d30ca3d5420f759d93151438bd77c8ba82855


These tests can be ran using the command:

./gradlew :java-selenium-dynamic-tests:test

./gradlew :java-selenium-dynamic-tests:phantomJsTest

## Selenium Grid ##

To run tests against Selenium Server Grid, setup a local hub and node. The Selenium Server can be downloaded from http://www.seleniumhq.org/download/

Create the local hub and grid by issuing the following commands in separate terminal:

```
#!bash

java -jar selenium-server-standalone-2.53.1.jar -role hub

java -jar selenium-server-standalone-2.53.1.jar -role node -hub http://localhost:4444/grid/register
```
Run the tests with default configurations using:

./gradlew :java-selenium-dynamic-tests:remoteTest

The tests will run against the local Selenium hub using Chrome. Use the java property **test.remote.url** to change the hub URL, or **test.remote.browser** to test with a different browser.

## Parallel and Parameterized Tests ##

The test runner in Gradle will run tests in parallel by setting **maxParallelForks** > 1. This spawns test classes in parallel.

Further, extending ConcurrentParameterizedTestBase allows running tests (within a class) in parallel along with parameterizing the tests to run against multiple browsers. A public, static method annotated with @ConcurrentParameterized.Parameters must exist to provide the parameters for the tests. To see an example, setup a hub with nodes having **chrome** and **safari** then run:

./gradlew :java-selenium-dynamic-tests:remoteConcurrentTest

The above example will run the LoginTests with both Chrome and Safari in parallel, while running PostTests parallel to these. Thus, you should see 2 Chrome browsers and 1 Safari browser open. The Safari browser and 1 Chrome browser run the LoginTests, while the remaining Chrome browser runs PostTests.

## Configuration ##

The tests will run with default configuration. To change configurations see the following properties:

**com.slalom.selenium.DriverConfiguration**

* test.driver: ChromeDriver, PhantomJs, Remote
* test.driver.chrome.location: specify file path to Chrome server
* test.driver.phantomjs.location: specify file path to PhantomJs server
* test.driver.implicit.wait.milliseconds: Set amount to implicitly wait. Default is zero.
* test.driver.remote.browser: Chrome, PhantomJs, Firefox, InternetExplorer
* test.driver.remote.url: URL for selenium grid

**com.slalom.selenium.TestConfiguration**

* test.site.address: URL for site to test against
* test.site.user: user to test with
* test.site.password: password of user
* test.screenshot.output: path to directory where screenshots should be stored.


## Dependencies ##

The frameworks and tests run against **Wordpress**. Use the link(s) below to install Wordpress locally:

**Mac**:
https://codex.wordpress.org/Installing_WordPress_Locally_on_Your_Mac_With_MAMP

**Windows**:
https://www.microsoft.com/web/wordpress

The tests have been ran using both the **Chrome** and **PhantomJs** drivers. The **Mac** version of the servers for these has been checked into the drivers directory. To test in a different environment download the appropriate driver and configure the appropriate java test.driver.[chrome, phantomjs].location property.